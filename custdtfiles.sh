#!/bin/bash

# installer has already created these dirs but hey
mkdir -p /usr/lib/custdesk/c
mkdir -p /usr/lib/custdesk/c/antix

# this script is called via /etc/apt/apt.conf.d/99-cust-desktopfiles
# following any package install|upgrade|remove|purge operation


cust="/usr/lib/custdesk/c/"
dest="/usr/share/applications/"
cd /usr/lib/custdesk/c
for dafile in *.desktop; do
    if [ -e $dest$dafile ] && ! cmp $dest$dafile $cust$dafile > /dev/null 2>&1; then
        cp $cust$dafile $dest
    fi
done
# in case any contain a customised list of mimetypes
update-desktop-database  --quiet /usr/share/applications

cust="/usr/lib/custdesk/c/antix/"
dest="/usr/share/applications/antix/"
cd /usr/lib/custdesk/c/antix
for dafile in *.desktop; do
    if [ -e $dest$dafile ] && ! cmp $dest$dafile $cust$dafile > /dev/null 2>&1; then
        cp $cust$dafile $dest
    fi
done
update-desktop-database  --quiet /usr/share/applications/antix

exit 0
